import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-alert',
  template: `
  	<p>{{ success }}</p>
  `,
  styles: [`
  p {
  	color: white;
  	background-color: green;
  }
  `]
})
export class SuccessAlertComponent implements OnInit {

	success = "Success message !"
	
	constructor() { }

	ngOnInit(): void {
	}
}
