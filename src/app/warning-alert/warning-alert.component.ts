import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  template: `
  	<p> {{ warning }} </p>
  `,
  styles: [`
  	p {
  		background-color: yellow;
  	}
  `]
})
export class WarningAlertComponent implements OnInit {

	warning = 'Warning message !'

	constructor() { }

	ngOnInit(): void {
	}

}
